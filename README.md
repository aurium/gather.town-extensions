Gather.Town Extensions
======================

Open the **[user script to add teleport UI](https://aurium.gitlab.io/gather.town-extensions/teleport-ui.user.js)** to your browser.

...or add this bookmarklet to your bookmarks:
<br><a href="javascript:d=document;s=d.createElement('script');s.onerror=(console.log);s.type='module';s.src='https://aurium.gitlab.io/gather.town-extensions/teleport-ui-extension.js?t='+(new%20Date()).toJSON().split('T')[0];void(d.head.appendChild(s))">Gather.Town Teleport UI</a>

