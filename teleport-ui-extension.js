/*
Add this bookmarklet to your browser's bookmarks:
javascript:d=document;s=d.createElement('script');s.onerror=(console.log);s.type='module';s.src='https://localhost:8080/teleport-ui-extension.js?t='+(new Date()).toJSON().split('T')[0];d.head.appendChild(s)
*/

let fnCounter = 0
function tryFunc(fnName, fn) {
    if (typeof(fnName) === 'function') {
        fn = fnName
        fnCounter++
        fnName = 'func'+fnCounter
    }
    return (...args)=> {
        try { return fn(...args) }
        catch(err) {
            console.log(`Function ${fnName} FAIL!`, err)
            throw err
        }
    }
}

//import mkEl from 'https://gitlab.com/-/snippets/1853886/raw/master/mkel.js'
const SVGNS = 'http://www.w3.org/2000/svg'

function mkEl(tag, attrs={}) {
  let el = (tag instanceof Element) ? tag : document.createElement(tag)

  mkEl.extend(el)

  Object.keys(attrs).forEach((att)=>
    att === 'text'
    ? el.appendChild(document.createTextNode(attrs[att]))
    : att === 'parent'
    ? attrs.parent.appendChild(el)
    : att === 'child'
    ? attrs.child.forEach((tag, i)=> {
      if (i%2 === 0) el.mkChild(tag, attrs.child[i+1]||{})
    })
    : att === 'css'
    ? el.setStyle(attrs.css)
    : att.match(/^on/)
    ? el[att] = attrs[att]
    : el.setAttribute(att, attrs[att])
  )

  return el
}

mkEl.svg = function mkSVGEl(tag, attrs) {
  if (tag instanceof Element)
    return mkEl(tag, attrs)
  else
    return mkEl(document.createElementNS(SVGNS, tag), attrs)
}

function mkChild(tag, attrs) {
  attrs = { ...attrs, parent: this }
  if (this instanceof SVGElement)
    return mkEl.svg(tag, attrs)
  else
    return mkEl(tag, attrs)
}

mkEl.extend = function extendElement(el) {
  el.mkChild = mkChild
  el.setStyle = (style)=> Object.entries(style).forEach(([key, val])=> el.style[key] = val )
  return el
}
/* * * * * * * * * * * * * * * * END mkEl * * * * * * * * * * * * * * * */

const d = document
const $ = (sel)=> d.querySelector(sel)
const $$ = (sel)=> Array.from(d.querySelectorAll(sel))
let tryIcon = null

mkEl('style', {
    parent: d.head,
    text: `
        #teleport-ctrl {
            min-width: 10em;
            min-height: 5em;
            position: absolute;
            bottom: 80px;
            color: #FFF;
            background: #252A48;
            box-shadow: rgba(0,0,0,.6) 0px 5px 20px;
            border-radius: 1.5em;
            padding: 1em;
        }
        #teleport-ctrl h3 {
            font-weight: bold;
            margin-bottom: 1em;
        }
        #teleport-ctrl .close {
            position: absolute;
            font-size: 2em;
            top: .3em;
            right: .3em;
            width: 1em;
            text-align: center;
            cursor: default;
        }
        #teleport-ctrl .bt-add {
            margin-top: .5em;
            padding: .2em;
            width: 100%;
            border: none;
            background: rgba(255,255,255,.1);
            border-radius: .5em;
            color: #FFF;
            font-weight: 'bold;
        }
        #teleport-ctrl .place-ctrl {
            display: flex;
            margin: 0 -.5em .5em 0;
        }
        #teleport-ctrl .place-ctrl .bt-go {
            width: 100%;
            text-align: left;
            padding: .3em .7em .3em 1em;
            border: 1px solid #000;
            border-top: 1px solid rgba(255,255,255,.3);
            border-left: 1px solid rgba(255,255,255,.3);
            background: rgba(255,255,255,.5);
            border-radius: .3em;
            color: #003;
        }
        #teleport-ctrl .place-ctrl .bt-rm {
            background: transparent;
            border: none;
        }
    `
})

const keyRE = /^teleport-place\t(.+)\t(.+)\t([0-9]+)\t([0-9]+)$/
const getSpaceId = tryFunc('getSpaceId', ()=> game.engine.spaceId)
const getMe = tryFunc('getMe', ()=> game.players[game.engine.clientUid])

const showTeleportCtrl = tryFunc('showTeleportCtrl', ()=> {
    if (tryIcon.disabled) return
    tryIcon.disabled = true
    const root = $('.GameComponent-container > .Layout')
    const btRect = tryIcon.getBoundingClientRect()
    const ctrlBox = mkEl('div', {
        parent: root,
        id: 'teleport-ctrl',
        css: { right: (root.clientWidth - btRect.left - btRect.width - 20)+'px' },
        child: ['h3', { text: 'Teleport' }]
    })
    const closeBt = ctrlBox.mkChild('div', {
        text: '×',
        class: 'close',
        onclick() {
            ctrlBox.remove()
            tryIcon.disabled = false
        }
    })
    const placesBox = ctrlBox.mkChild('div', {})
    const addBtn = ctrlBox.mkChild('button', {
        text: 'Add this place',
        class: 'bt-add',
        onclick() {
            const {x, y, map} = getMe()
            const placeName = prompt('Enter the place name:')
            if (placeName) {
                const k = `teleport-place\t${getSpaceId()}\t${map}\t${x}\t${y}`
                localStorage[k] = placeName
                rebuildTeleportCtrl()
            }
        }
    })
    Object.keys(localStorage)
    .filter(k => {
        const match = k.match(keyRE)
        return match && match[1] === getSpaceId()
    })
    .forEach(k => mkPlaceCtrl(placesBox, k))
})

const mkPlaceCtrl = tryFunc('mkPlaceCtrl', (placesBox, key)=> {
    placesBox.mkChild('div', {
        class: 'place-ctrl',
        child: [
            'button', {
                text: localStorage[key],
                class: 'bt-go',
                onclick() {
                    const [_, spaceId, map, x, y] = key.match(keyRE)
                    game.teleport(map,x,y,getMe().id)
                }
            },
            'button', {
                text: '🗑️',
                class: 'bt-rm',
                onclick() {
                    delete localStorage[key]
                    rebuildTeleportCtrl()
                }
            }
        ]
    })
})

const rebuildTeleportCtrl = tryFunc('rebuildTeleportCtrl', ()=> {
    $('#teleport-ctrl .close').click()
    showTeleportCtrl()
})

tryFunc('INIT', ()=> {
    console.log('AEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', mkEl)
    const sysTray = $('.GameComponent-container > .Layout > div:last-child > div:last-child')
    tryIcon = mkEl('button', {
        text: 'Teleport',
        onclick: showTeleportCtrl
    })
    sysTray.firstChild.before(tryIcon)
})()



