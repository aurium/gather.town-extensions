// ==UserScript==
// @name     Gather.Town Teleport UI
// @version  0.1.0
// @author   Aurélio A. Heckert
// @match    https://app.gather.town/*
// @grant    none
// updateURL https://aurium.gitlab.io/gather.town-extensions/teleport-ui.user.js
// ==/UserScript==

const script = document.createElement('script')
const now = (new Date()).toJSON().split('T')[0]
script.onerror=(console.log)
script.type='module'
script.src='https://aurium.gitlab.io/gather.town-extensions/teleport-ui-extension.js?t='+now
document.head.appendChild(script)
